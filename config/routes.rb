Rails.application.routes.draw do
  ##### PAGES
  root to:"pages#home"
  get "/home", to: "pages#home" , as:"principal"
  get "/casa", to: "pages#casa"
  get "/franquicias/:nombre", to: "pages#tiendas", as:"franquicias"
  #####
  ##### ENTRADAS
  resources :entradas, path_names: { new: 'nuevo'}
  get "/entradas/:id/destroy", to: "entradas#destroy", as: "entrada_destroy"
  #####
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
end

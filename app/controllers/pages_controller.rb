class PagesController < ApplicationController
    layout "pagesLayout"
    def home
        @nombre="Paco"
    end
    def tiendas
        @tienda=params[:nombre]
        @tiendas=["Tienda BCN","Tienda VLC"]
    end
    def casa
        redirect_to principal_url
    end
end

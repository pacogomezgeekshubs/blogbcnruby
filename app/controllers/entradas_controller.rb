class EntradasController < ApplicationController
    #listado de todas las entradas
    def index
        @entradas = Entrada.all
      end
    #Visualizar una entrada con Id
    def show
        @entrada=Entrada.find(params[:id])
    end
    #Presentamos el FORMULARIO PARA UN EDIT
    def edit
        @entrada = Entrada.find(params[:id])
    end
    #Presentamos el FORMULARIO PARA UN INSERT
    def new
        @entrada = Entrada.new
    end
    #Recogemos los datos PARA EL INSERT a partir del new
    def create
        @entrada= Entrada.new(entrada_params)
        if @entrada.save
            redirect_to @entrada
        else
            render "new"
        end
    end 
    #Recogemos los datos PARA EL INSERT a partir del new
    def update
        @entrada= Entrada.find(params[:id])
        @entrada.update(entrada_params)
        redirect_to @entrada
    end
    def destroy
        @entrada = Entrada.find(params[:id])
        @entrada.destroy
     
        redirect_to entradas_path
    end
    #metodos internos
    private
    def entrada_params
        params.require(:entrada).permit(:titulo, :texto)
    end
end
